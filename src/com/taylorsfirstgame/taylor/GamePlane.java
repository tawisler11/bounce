package com.taylorsfirstgame.taylor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GamePlane extends SurfaceView implements Runnable{

	static SurfaceHolder surfaceHolder;
	static Boolean isRunning,oneTime;
	Thread ourThread;
	static Bitmap test;
	Bitmap exx, score, gameover;
	static int screenWidth;
	static int screenHeight;
	private Bitmap[] numArr;
	private int thousandScore, hundredScore, tenScore, oneScore;
	
	public GamePlane(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		surfaceHolder = getHolder();
		test = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
		exx = BitmapFactory.decodeResource(getResources(), R.drawable.exx);
		score = BitmapFactory.decodeResource(getResources(), R.drawable.score);
		gameover = BitmapFactory.decodeResource(getResources(), R.drawable.gameover);
		
		Bitmap a0,a1,a2,a3,a4,a5,a6,a7,a8,a9;
		a0 = BitmapFactory.decodeResource(getResources(), R.drawable.a0);
		a1 = BitmapFactory.decodeResource(getResources(), R.drawable.a1);
		a2 = BitmapFactory.decodeResource(getResources(), R.drawable.a2);
		a3 = BitmapFactory.decodeResource(getResources(), R.drawable.a3);
		a4 = BitmapFactory.decodeResource(getResources(), R.drawable.a4);
		a5 = BitmapFactory.decodeResource(getResources(), R.drawable.a5);
		a6 = BitmapFactory.decodeResource(getResources(), R.drawable.a6);
		a7 = BitmapFactory.decodeResource(getResources(), R.drawable.a7);
		a8 = BitmapFactory.decodeResource(getResources(), R.drawable.a8);
		a9 = BitmapFactory.decodeResource(getResources(), R.drawable.a9);
		numArr = new Bitmap[]{a0,a1,a2,a3,a4,a5,a6,a7,a8,a9};
		
		oneTime=false;
	}
	
	public void pause(){
		isRunning = false;
		while(true){
			try {
				ourThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
		ourThread = null;
	}
	public void resume(){
		isRunning = true;
		ourThread = new Thread(this);
		ourThread.start();
	}
	
	private void calcScoreNums(){
		thousandScore = com.taylorsfirstgame.taylor.Game.score/1000;
	    hundredScore = (com.taylorsfirstgame.taylor.Game.score%1000)/100;
	    tenScore = (com.taylorsfirstgame.taylor.Game.score%100)/10 ;
	    oneScore = com.taylorsfirstgame.taylor.Game.score%10;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(isRunning){
			if(!surfaceHolder.getSurface().isValid())
				continue;
			Canvas canvas = surfaceHolder.lockCanvas();
			if(!oneTime){
				oneTime=true;
				screenWidth = canvas.getWidth();
				screenHeight = canvas.getHeight();
				com.taylorsfirstgame.taylor.Game.newPlatform(500, screenWidth);
				com.taylorsfirstgame.taylor.Game.newPlatform(1000, screenWidth);
				com.taylorsfirstgame.taylor.Game.newPlatform(1500, screenWidth);
			}
			Paint paint = new Paint();
			paint.setARGB(255, 255, 255, 255);
			
			//==================
			//	Background
			//==================
			canvas.drawRGB(01,02,150);
			
			//==================
			//	platforms
			//==================
			for(int i = 0; i < com.taylorsfirstgame.taylor.Game.platformVector.size(); i++)
			{
				canvas.drawRect(com.taylorsfirstgame.taylor.Game.platformVector.get(i).getX(), com.taylorsfirstgame.taylor.Game.platformVector.get(i).getY(), com.taylorsfirstgame.taylor.Game.platformVector.get(i).getEndX(), com.taylorsfirstgame.taylor.Game.platformVector.get(i).getY()+20, paint);
			}
			
			//==================
			//	X
			//==================
			calcScoreNums();
			//canvas.drawBitmap(exx, com.taylorsfirstgame.taylor.Game.x-(test.getWidth()/2), com.taylorsfirstgame.taylor.Game.y-(test.getHeight()/2), null);
			
			canvas.drawBitmap(score, 0,0, null);
			canvas.drawBitmap(numArr[oneScore], score.getWidth()+100 + numArr[0].getWidth()*3,0, null);
			if(com.taylorsfirstgame.taylor.Game.score >= 10)
				canvas.drawBitmap(numArr[tenScore], score.getWidth()+100 + numArr[0].getWidth()*2,0, null);
			if(com.taylorsfirstgame.taylor.Game.score >= 100)
				canvas.drawBitmap(numArr[hundredScore], score.getWidth()+100 + numArr[0].getWidth(),0, null);
			if(com.taylorsfirstgame.taylor.Game.score >= 1000)
				canvas.drawBitmap(numArr[thousandScore], score.getWidth()+100,0, null);
				
				
			
			//==================
			//	Ball
			//==================
			canvas.drawBitmap(test, com.taylorsfirstgame.taylor.Game.bx-(test.getWidth()/2), com.taylorsfirstgame.taylor.Game.by-(test.getHeight()/2), null);
			
			//=================
			//		DEAD
			//=================
			if(com.taylorsfirstgame.taylor.Game.gameState == com.taylorsfirstgame.taylor.Game.State.DEAD)
			{
				canvas.drawBitmap(gameover, (screenWidth/2)-gameover.getWidth()/2, (screenHeight/2)-gameover.getHeight()/2, null);
			}
			
			
			
			surfaceHolder.unlockCanvasAndPost(canvas);
			
		}
	}

}
