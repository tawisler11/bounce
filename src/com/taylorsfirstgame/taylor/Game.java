package com.taylorsfirstgame.taylor;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.content.Context;
//import android.content.Intent;
//import android.widget.*;

public class Game extends Activity implements OnTouchListener, SensorEventListener{

	static GamePlane gamePlane;
	static int x,y,dy,dx,bx,by,score;
	private final int gravity = 1;
	private final int jump = -20;
	private Timer myTimer;
	private int counter;
	
	static Vector<platform> platformVector;
	
	private float mLastX, mLastY, mLastZ;
	private boolean mInitialized;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private final float NOISE = (float)2.0;
	static platform Platform = new platform();
	
	public enum State{
		MENU,
		PLAYING,
		DEAD
	}
	
	static State gameState;
	
	//TextView txtView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		gamePlane = new GamePlane(this);
		gamePlane.setOnTouchListener(this);
		setContentView(gamePlane);
		
		platformVector = new Vector<platform>();
		
		bx=500;
		by=500;//gamePlane.getHeight()-com.taylorsfirstgame.taylor.GamePlane.test.getHeight();
		dy=0;
		dx=0;
		x = 500;
		y = 1000;
		counter = 0;
		score = 0;
		gameState = State.PLAYING;
		
		mInitialized = false;
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
		
		myTimer = new Timer();
		myTimer.scheduleAtFixedRate(new TimerTask(){
			@Override
			public void run(){
				moveBall();
			}
		}, 1000, 10);
		
		//Platform.generatePlatform(1000, 400);
	}
	public static void newPlatform(int tempy, int tempwidth){
		platform temp = new platform();
		temp.generatePlatform(tempy, tempwidth);
		platformVector.add(temp);
	}
	
	private void bounce(){
		//dy *= -0.8; // depletion bounce
		dy = jump;
	}
	
	private TimerTask moveBall(){
		
		if(gameState == State.PLAYING)
		{
		
		//============================
		// 		Gravity Mechanics
		//============================
		counter++;
		if (counter == 5)
		{
			counter = 0;
			dy += gravity;
		}
		
		//============================
		// 		MoveBall
		//============================
		
		by += dy;
		bx += dx;
		
		//============================
		//      ceiling / floor
		//============================
		
		if(by+(com.taylorsfirstgame.taylor.GamePlane.test.getHeight()/2) >= gamePlane.getHeight()) //Hits "bottom"
		{
			by = gamePlane.getHeight()-com.taylorsfirstgame.taylor.GamePlane.test.getHeight()/2; //ball just above bottom
			//Intent openmenu = new Intent(Game.this, Menu.class);
			//finish();
			//bounce();
			gameState = State.DEAD;
			return null;
		}else if(by <= com.taylorsfirstgame.taylor.GamePlane.test.getHeight()) //Hits "ceiling"
		{
			by = com.taylorsfirstgame.taylor.GamePlane.test.getHeight();
			for(int i = 0; i < platformVector.size(); i++)
			{
				platformVector.get(i).setY(platformVector.get(i).getY() - dy);
				if(platformVector.get(i).getY() > gamePlane.getHeight()){
					platformVector.removeElementAt(i);
					i--;
				}
			}
			if(platformVector.lastElement().getY() > 500 && com.taylorsfirstgame.taylor.GamePlane.oneTime){
				score++;
				newPlatform(0, com.taylorsfirstgame.taylor.GamePlane.screenWidth);
			}
		}
		
		//============================
		//  		Platforms
		//============================
		
		for(int i = 0; i < platformVector.size(); i++)
		{
			if(bx >= platformVector.get(i).getX() && bx <= platformVector.get(i).getEndX() && by <= platformVector.get(i).getY() && by + com.taylorsfirstgame.taylor.GamePlane.test.getHeight()/2 >= platformVector.get(i).getY() && dy > 0 ) //Hits "Left wall"
			{
				by = platformVector.get(i).getY()-com.taylorsfirstgame.taylor.GamePlane.test.getHeight()/2; //ball just above bottom
				bounce();
			}
		}
		
		
		//============================
		// 			Walls
		//============================
		
		if(bx+(com.taylorsfirstgame.taylor.GamePlane.test.getWidth()/2) >= gamePlane.getWidth()) //Hits "Right wall"
		{
			bx = gamePlane.getWidth()-com.taylorsfirstgame.taylor.GamePlane.test.getWidth()/2;
			dx = 0;
		}else if(bx <= com.taylorsfirstgame.taylor.GamePlane.test.getWidth()/2) //Hits "Left wall"
		{
			bx = com.taylorsfirstgame.taylor.GamePlane.test.getWidth()/2;
			dx = 0;
		}
		
		}
		
		return null;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mSensorManager.unregisterListener(this);
		gamePlane.pause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
		gamePlane.resume();
	}
	
	private void restart(){
		score = 0;
		bx = 500;
		by = 500;
		dy = 0;
		dx = 0;
		gameState = State.PLAYING;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(gameState == State.DEAD)
		{
			switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				restart();
				//x = (int) event.getX();
				//y = (int) event.getY();
			}
		}
		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		
		float xA = event.values[0];
		float yA = event.values[1];
		float zA = event.values[2];
		if (!mInitialized) {
			mLastX = xA;
			mLastY = yA;
			mLastZ = zA;
			
			mInitialized = true;
		} else {
			float deltaX = Math.abs(mLastX - xA);
			float deltaY = Math.abs(mLastY - yA);
			float deltaZ = Math.abs(mLastZ - zA);
			if (deltaX < NOISE) deltaX = (float)0.0;
			if (deltaY < NOISE) deltaY = (float)0.0;
			if (deltaZ < NOISE) deltaZ = (float)0.0;
			mLastX = xA;
			mLastY = yA;
			mLastZ = zA;
		
			if (xA > 0.5) {
				dx = (int)(-3.0*xA);
			}else if (xA < -0.5){
				dx = (int)(3.0*(-xA));
			}else{
				dx = 0;
			}
		}
		
	}
	
}
